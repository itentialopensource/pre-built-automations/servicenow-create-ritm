
## 0.2.9 [01-03-2024]

* Removes relative links

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!21

---

## 0.2.8 [01-03-2024]

* Remove img tag from markdown file

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!17

---

## 0.2.7 [09-22-2023]

* add deprecation information

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!16

---

## 0.2.6 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!15

---

## 0.2.5 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!14

---

## 0.2.4 [02-14-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!13

---

## 0.2.3 [11-15-2021]

* patch/dsup 999

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!12

---

## 0.2.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!11

---

## 0.2.1 [03-19-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!10

---

## 0.2.0 [06-18-2020]

* Add './' to img path

See merge request itentialopensource/pre-built-automations/servicenow-create-ritm!5

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab

See merge request itentialopensource/pre-built-automations/ServiceNow-Create-RITM!4

---

## 0.0.2 [05-29-2020]

* Update package.json, manifest.json, bundles/workflows/ServiceNow Create RITM...

See merge request itentialopensource/pre-built-automations/staging/ServiceNow-Create-RITM!3

---\n\n\n\n\n
