<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 08-24-2023 and will be end of life on 06-30-2024. The capabilities of this Pre-Built have been replaced by the [Itential ServiceNow - Now Platform - REST Workflow Project](https://gitlab.com/itentialopensource/pre-built-automations/servicenow-now-platform-rest)

<!-- Update the below line with your pre-built name -->
# ServiceNow Order Service Catalog Item (Create RITM)

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [ServiceNow Order Service Catalog Item (Create RITM)](#servicenow-order-service-catalog-item-create-ritm)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
    - [Operations Manager and JSON-Form](#operations-manager-and-json-form)
    - [Main Workflow](#main-workflow)
    - [Error Handling](#error-handling)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Requirements](#requirements)
  - [Features](#features)
  - [Future Enhancements](#future-enhancements)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
  - [Additional Information](#additional-information)

## Overview

This Pre-built integrates with the [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow) to create a Request and a Request Item (RITM) using the Service Catalog API.


### Operations Manager and JSON-Form

This Pre-Built has the Operations Manager Automation `ServiceNow Create RITM (Service Catalog Order)` that calls the workflow `ServiceNow Create RITM (Service Catalog Order) - Automation Catalog`. The Automation uses a JSON-Form to specify the `sys_id` of the Operations Manager item, as well as any variables needed to create the RITM. The workflow the Automation Item calls queries data from the `formData` job variable.

### Main Workflow

The main workflow in this pre-built is the workflow `ServiceNow Create RITM (Service Catalog Order`, which takes in several input variables required to create a change.

_Estimated Run Time_: 30 seconds

### Error Handling

If any task in the workflow fails to perform its expected function, the `taskError` job variable will hold the error message. If you are using any workflow in this pre-built as a childJob, you can use the `eval` task in Workflow Engine to check if the value of `taskError` is empty. If it is not empty, then the ServiceNow change that was created encountered an error.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`
- [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow)
  - `^2.2.0`

## Requirements

This pre-built requires the following:

- A [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow).
- A ServiceNow account that has access to order a Service Catalog Item.
- A ServiceNow account that has read access from the `sc_request` and `sc_req_item` tables.

## Features

The main benefits and features of the pre-built are outlined below.

- Place a Service Catalog order to create a Request and a Request Item (RITM).
- Query the RITM Number after an order is placed and a Request is created.
- A workflow to retrieve variables from a Service Catalog Item.

## Future Enhancements

The following is planned for a future release of this pre-built:

- Dynamically create a form using the variables from a Service Catalog Item.

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. If you do not currently have Admin Essentials installed on your server, please download the installer from your Nexus repository. Refer to the install instructions included in the Admin Essentials README.
* The pre-built can be installed from within Admin Essentials. Simply search for the name of your desired pre-built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the pre-built:

* You must first retrieve the name of the variables required to create the Request. Go to ServiceNow and locate the name of the Request Item you wish to order.

* Run the workflow `ServiceNow Display RITM variables` and input the RITM name.

* Run the worfklow `ServiceNow Create RITM (Service Catalog Order)` with the name of your variables and SYSID.

_Example_

```json
  {
   "sysIdOfServiceCatalogItem": "186d917a6fab7980575967ddbb3ee4f2",
   "ritmVariables": {"new_email": "myEmail@email.com"}
  }
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
